// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AArenaCharacter::AArenaCharacter(const FObjectInitializer& ObjInit) 
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}

// Called when the game starts or when spawned
void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AArenaCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateMouseAim(DeltaSeconds);
	MovementTick(DeltaSeconds);
}

// Called to bind functionality to input
void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AArenaCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AArenaCharacter::InputAxisY);
	
	
}

void AArenaCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AArenaCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void AArenaCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
}


FVector2D AArenaCharacter::GetMouseVelocity()
{
	APlayerController* PC = Cast<APlayerController>(GetController());

	float DeltaX = 0.0f;
	float DeltaY = 0.0f;

	if (PC == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("PController is NULL."))
	}

	PC->GetInputMouseDelta(DeltaX, DeltaY);
	FVector2D MouseDelta = UKismetMathLibrary::MakeVector2D(DeltaX, DeltaY);
	UE_LOG(LogTemp, Warning, TEXT(" X: %f , Y: %f"), MouseDelta.X, MouseDelta.Y);
	FVector2D ReturnVector = MouseDelta / UGameplayStatics::GetWorldDeltaSeconds(GetWorld());


	return ReturnVector;
}

FRotator AArenaCharacter::GetMouseAimDirection(float DeltaSecond)
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	FRotator PCRot = PC->GetControlRotation();
	FRotator MouseRot = UKismetMathLibrary::MakeRotator(0.0f, 0.0f, UKismetMathLibrary::DegAtan2(GetMouseVelocity().X, GetMouseVelocity().Y));

	FRotator ReturnRot = FMath::RInterpTo(PCRot, MouseRot, DeltaSecond, MouseSmoothingStrenght);

	return ReturnRot;
}

bool AArenaCharacter::IsMouseAboveDeadZone()
{
	float Vector2dLength = UKismetMathLibrary::VSize2D(GetMouseVelocity());

	return Vector2dLength > MouseDeadZone;
}

void AArenaCharacter::UpdateMouseAim(float DeltaSecond)
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (IsMouseAboveDeadZone())
	{
		PC->SetControlRotation(GetMouseAimDirection(DeltaSecond));
	}
}

