// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ArenaCharacter.generated.h"

UCLASS()
class GB_ARENA_API AArenaCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	AArenaCharacter(const FObjectInitializer& ObjInit);

	virtual void Tick(float DeltaSeconds) override;

	void BeginPlay();
	
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	

public:
	
UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	
	
	float AxisX = 0.0f;
    float AxisY = 0.0f;

	UFUNCTION(BlueprintCallable)
	void MovementTick(float DeltaTime);
   
    	
	float MouseDeadZone = 5.0f; 
	float MouseSmoothingStrenght = 2.5f; 

	FVector2D GetMouseVelocity(); 
	FRotator GetMouseAimDirection(float DeltaSecond); 
	bool IsMouseAboveDeadZone(); 

	UFUNCTION(BlueprintCallable)
		void UpdateMouseAim(float DeltaSecond);
		
};
